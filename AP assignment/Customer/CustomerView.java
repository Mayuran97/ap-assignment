package demo.BookStore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CustomerView extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerView frame = new CustomerView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CustomerView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 638, 467);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Welcome");
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel.setBounds(257, 11, 85, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Buy Books");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				BookDetails BD = new BookDetails();
				BD.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnNewButton.setBounds(123, 68, 113, 23);
		contentPane.add(btnNewButton);
		
		JButton btnBuyMusicCd = new JButton("Buy Music CD");
		btnBuyMusicCd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				CDdetails CDd = new CDdetails();
				CDd.setVisible(true);
			}
		});
		btnBuyMusicCd.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnBuyMusicCd.setBounds(118, 159, 140, 23);
		contentPane.add(btnBuyMusicCd);
		
		JButton btnBuySoftwares = new JButton("Buy Softwares");
		btnBuySoftwares.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				SoftwareDetails SD = new SoftwareDetails();
				SD.setVisible(true);
			}
		});
		btnBuySoftwares.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnBuySoftwares.setBounds(118, 117, 129, 23);
		contentPane.add(btnBuySoftwares);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\mayuran\\Downloads\\08d631af6c78d9940d5c6b5894dae288--vector-design-design-templates.jpg"));
		lblNewLabel_1.setBounds(368, 23, 219, 195);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\mayuran\\Downloads\\cd-music-clipart-1.jpg"));
		lblNewLabel_2.setBounds(31, 214, 231, 203);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				CustomerLogin CL = new CustomerLogin();
				CL.setVisible(true);
			}
		});
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnNewButton_1.setBounds(484, 313, 89, 23);
		contentPane.add(btnNewButton_1);
	}

	public static int delete(int customer_id) {
		// TODO Auto-generated method stub
		return 0;
	}
}
